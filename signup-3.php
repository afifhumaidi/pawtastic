<!DOCTYPE html>
<html lang="en">

<head>
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign-up</title>

    <link rel="icon" href="assets/img/Group247.svg">
    <link rel="stylesheet" href="assets/css/signup.css">
    <!-- Font -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Basic&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">
</head>

<body>
    <div class="container">
        <div class="aside">
            <img class="aside__bg" src="assets/img/signup-2.jpg">
            <div class="aside__container">
                <div class="aside__logo mb-66">
                    <div class="aside__circle"><img class="aside__icon" src="assets/img/Group247.svg"></div>
                    <p class="aside__title">PAWTASTIC</p>
                </div>

                <h2 class="aside__title aside__title_list">Why our service?</h2>
                <ul class="aside__list">
                    <li>We're animal lovers backed by insurance and experience</li>
                    <li>Powered by tech, so you can book and pay from our app</li>
                    <li>Updates and pics for every visit keep you in the loop</li>
                </ul>
            </div>
        </div>

        <div class="section">
            <div class="section__container">
                <h1 class="section__title mb-44">Hello! Please tell us a little bit about yourself.</h1>
                <form action="#">
                    <div class="grid section__grid section__grid_col-2">
                        <div class="section__grid-item">
                            <label class="section__label" for="first-name">First name</label>
                            <input class="section__input section__input_col-2" type="text" id="first-name" name="first-name" placeholder="Your first name" onwheel="event.target.blur()">
                        </div>
                        <div class="section__grid-item">
                            <label class="section__label" for="last-name">Last name</label>
                            <input class="section__input section__input_col-2" type="text" id="last-name" name="last-name" placeholder="Your last name" onwheel="event.target.blur()">
                        </div>
                        <div class="section__grid-item mt-22">
                            <label class="section__label" for="phone">Phone</label>
                            <input class="section__input section__input_col-2" type="text" id="phone" name="phone" placeholder="000-000-0000" onwheel="event.target.blur()">
                        </div>
                        <div class="section__grid-item mt-22">
                            <label class="section__label" for="phone-alt">Phone alt</label>
                            <input class="section__input section__input_col-2" type="text" id="phone-alt" name="phone-alt" placeholder="000-000-0000" onwheel="event.target.blur()">
                        </div>
                    </div>
                    <div class="mt-22">
                        <label class="section__label" for="label">Label</label>
                        <input class="section__input section__input_col-1" type="text" id="label" name="label" placeholder="Placeholder text" onwheel="event.target.blur()">
                    </div>

                    <div class="grid section__grid section__grid_col-3 mt-22">
                        <div class="section__grid-item">
                            <label class="section__label" for="city">City</label>
                            <input class="section__input section__input_col-2" type="text" id="city" name="city" placeholder="City" onwheel="event.target.blur()">
                        </div>
                        <div class="section__grid-item">
                            <label class="section__label" for="state">State</label>
                            <input class="section__input section__input_col-3" type="text" id="state" name="state" placeholder="State" onwheel="event.target.blur()">
                        </div>
                        <div class="section__grid-item">
                            <label class="section__label" for="zip">Zip</label>
                            <input class="section__input section__input_col-3" type="number" id="zip" name="zip" placeholder="32789" onwheel="event.target.blur()">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="navigation">
            <div class="navigation__container">
                <a class="back" href="signup-2.php">
                    <div class="navigation__button">back</div>
                </a>
                <a class="next" href="signup-4.php">
                    <div class="navigation__button">next</div>
                </a>
            </div>
        </div>
</body>

</html>
