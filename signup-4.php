<!DOCTYPE html>
<html lang="en">

<head>
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign-up</title>

    <link rel="icon" href="assets/img/Group247.svg">
    <link rel="stylesheet" href="assets/css/signup.css">
    <!-- Font -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Basic&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">
</head>

<body>
    <div class="container">
        <div class="aside">
            <img class="aside__bg" src="assets/img/signup-2.jpg">
            <div class="aside__container">
                <div class="aside__logo mb-66">
                    <div class="aside__circle"><img class="aside__icon" src="assets/img/Group247.svg"></div>
                    <p class="aside__title aside__title_logo">PAWTASTIC</p>
                </div>
                
                <ul class="aside__list">
                    <li>Human profile</li>
                    <li>Pet basics</li>
                    <li>Pet details</li>
                    <li>Confirm</li>
                </ul>
            </div>
        </div>

        <div class="section">
            <div class="section__container">
                <h1 class="section__title mb-50">Nice to meet you, Meagan. Tell us all about your furry, feathery, or scaley friend.</h1>
                
                <label class="section__label" for="label">Label</label>
                <div class="option section__option mt-19">
                    <div class="section__option-item section__option-item_selected">
                        <img src="assets/img/dogo.jpg" alt="option-1">
                        <h2>Option 1</h2>
                    </div>
                    <div class="section__option-item">
                        <img src="assets/img/cat-2.jpg" alt="option-1">
                        <h2>Option 2</h2>
                    </div>
                    <div class="section__option-item">
                        <img src="assets/img/birdy2.jpg" alt="option-1">
                        <h2>Option 3</h2>
                    </div>
                    <div class="section__option-item">
                        <img src="assets/img/hamster.jpg" alt="option-1">
                        <h2>Option 4</h2>
                    </div>
                </div>
                <p class="mt-50">Have multiple pets? That's awesome. You can create additional pet profiles for the whole family later.</p>
            </div>
        </div>
    </div>
    <div class="navigation">
            <div class="navigation__container">
                <a class="back" href="signup-3.php">
                    <div class="navigation__button">back</div>
                </a>
                <a class="next" href="signup-5.php">
                    <div class="navigation__button">next</div>
                </a>
            </div>
        </div>
</body>
<script>
$('input[type="checkbox"]').on('change', function() {
    $('input[type="checkbox"]').not(this).prop('checked', false);
});
</script>
</html>

<!-- Note -->
<!-- Option still not done yet-->
<!-- Milestone no done yet -->