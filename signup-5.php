<!DOCTYPE html>
<html lang="en">

<head>
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign-up</title>

    <link rel="icon" href="assets/img/Group247.svg">
    <link rel="stylesheet" href="assets/css/signup.css">
    <!-- Font -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Basic&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">
</head>

<body>
    <div class="container">
        <div class="aside">
            <img class="aside__bg" src="assets/img/signup-2.jpg">
            <div class="aside__container">
                <div class="aside__logo mb-66">
                    <div class="aside__circle"><img class="aside__icon" src="assets/img/Group247.svg"></div>
                    <p class="aside__title aside__title_logo">PAWTASTIC</p>
                </div>
                
                <ul class="aside__list">
                    <li>Human profile</li>
                    <li>Pet basics</li>
                    <li>Pet details</li>
                    <li>Confirm</li>
                </ul>
            </div>
        </div>

        <div class="section">
            <div class="section__container">
                <h1 class="section__title mb-44">Yay, we love dogs! Give us the basics about your pup.</h1>
                <form action="#">
                    <div class="grid section__grid section__grid_col-2">
                        <div class="section__grid-item">
                            <label class="section__label" for="name">Name</label>
                            <input class="section__input section__input_col-2" type="text" id="name" name="name" placeholder="Pet's name" onwheel="event.target.blur()">
                        </div>
                        <div class="section__grid-item photo">
                            <div class="photo">
                                <img src="assets/img/Group247.svg">
                                <label class="section__label" for="last-name">Last name</label>
                            </div>
                        </div>
                        <div class="section__grid-item mt-22">
                            <label class="section__label" for="breed">Breed</label>
                            <input class="section__input section__input_col-2" type="text" id="breed" name="breed" placeholder="Pet's breed" onwheel="event.target.blur()">
                        </div>
                        <div class="section__grid-item mt-22">
                            <label class="section__label" for="birthday">Birthday</label>
                            <input class="section__input section__input_col-2" type="text" id="birthday" name="birthday" placeholder="MM/DD/YYYY" onwheel="event.target.blur()">
                        </div>
                        <div class="section__grid-item mt-22">
                            <label class="section__label" for="gender">Gender</label>
                            <input class="section__input section__input_col-2" type="text" id="gender" name="gender" placeholder="000-000-0000" onwheel="event.target.blur()">
                        </div>
                        <div class="section__grid-item mt-22">
                            <label class="section__label" for="spayed">Spayed or Neutered</label>
                            <input class="section__input section__input_col-2" type="text" id="spayed" name="spayed" placeholder="000-000-0000" onwheel="event.target.blur()">
                        </div>
                    </div>
                    <div class="mt-22">
                        <label for="weight">Weight</label>
                        <input class="section__input section__input_col-1" type="text" id="weight" name="weight" placeholder="Placeholder text" onwheel="event.target.blur()">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="navigation">
            <div class="navigation__container">
                <a class="back" href="signup-4.php">
                    <div class="navigation__button">back</div>
                </a>
                <a class="next" href="signup-6.php">
                    <div class="navigation__button">next</div>
                </a>
            </div>
        </div>
</body>

</html>
