<!DOCTYPE html>
<html lang="en">
<head>
<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>

    <link rel="icon" href="assets/img/Group247.svg">
    <link rel="stylesheet" href="assets/css/index.css">
    
    <!-- Font -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Basic&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">
</head>

<body>
    <div id="container" class="container">
        <div class="section1">
            <img id="section1__bg" class="section1__bg" src="assets/img/bg.jpg">
    
            <div class="logo">
                <div class="logo__circle"><img class="logo__img" src="assets/img/Group247.svg"></div>
                <p class="logo__title">PAWTASTIC</p>
            </div>
            
            <div class="intro">
                <div class="intro__navbar">
                    <a href="#">About Us</a>
                    <a href="#">Reviews</a>
                    <a href="#">Services</a>
                    <a href="signup-1.php">Sign Up</a>
                </div>
                <div class="intro__greeting">
                    <p>We care for your furry little loved ones while you're away</p>
                </div>
                <a href="#" class="intro__button">Schedule a visit</a>
            </div>
        </div>

        <div class="section2">
            <div class="left-content">
                <p class="left-content__title">Expert care for your furry, feathery, or scaley friend</p>
                <p>We know how stressful it is to leave your pets at home alone. We're a team of experienced animal caregivers, well connected to local veterinarians. Trust to us to love them like our own, and to keep them safe and happy till you're home.</p>
                <a href="#" class="left-content__button">Schedule a visit</a>
            </div>
            <div class="right-content grid">
                <div class="grid__item"><img src="assets/img/content1-1.jpg"><p>Muffin</p></div>
                <div class="grid__item"><img src="assets/img/content1-2.jpg"><p>Peep</p></div>
                <div class="grid__item"><img src="assets/img/content1-3.jpg"><p>Natasha</p></div>
                <div class="grid__item"><img src="assets/img/content1-4.jpg"><p>Marlon</p></div>
            </div>
        </div>

        <div class="section3">
            <div class="left-content"><img src="assets/img/content2-1.jpg"><p>Ollie and Maggie</p></div>
            <div class="right-content">
                <div class="right-content__wrapper">
                    <p class="right-content__title">Services tailored to your needs</p>
                    <p>Schedule one-off or recurring home visits. An experienced member of our team will spend time with your pet, feed them, change cat litter trays, take the dog for walk, and anything else you need.</p>
                    <a href="#" class="right-content__button">Schedule a visit</a>
                </div>
            </div>
        </div>

        <div class="section4">
            <p class="section4__title">Pets (and their humans) love us</p>
            <div class="section4__wrapper grid">
                <div class="grid__item">
                    <img src="assets/img/lindsay.jpg">
                    <div class="reviews">
                        <p class="reviews__name">Lindsay M.</p>
                        <p class="reviews__text">"Pawtastic is awesome! They are passionate about pets and employ trustworthy, dependable staff. We love them!"</p>
                    </div>
                </div>
                <div class="grid__item">
                    <img src="assets/img/floof.jpg">
                    <div class="reviews">
                        <p class="reviews__name">Andrew C.</p>
                        <p class="reviews__text">"I'm a repeat customer because of their amazing care for our two cats when we travel. I can relax because I know they're there!"</p>
                    </div>
                </div>
                <div class="grid__item">
                    <img src="assets/img/ginger.jpg">
                    <div class="reviews">
                        <p class="reviews__name">Meg F.</p>
                        <p class="reviews__text">"I use them for mid day walks and our babies are so happy with the exercise and love during the day. We see the difference!"</p>
                    </div>
                </div>
                <div class="grid__item">
                    <img src="assets/img/ned.jpg">
                    <div class="reviews">
                        <p class="reviews__name">Jackie B.</p>
                        <p class="reviews__text">"I just returned from two weeks away to a sociable, calm cat and no drama. Thanks for a great job, Pawtastic!"</p>
                    </div>
                </div>
            </div>
            <a href="#" class="section4__button">Read all reviews</a>
        </div>

        <div class="section5">
            <p class="section5__title">Affordable options, tailored to your needs</p>
            <p class="section5__text">All services include live updates including photos and chat, as well as notifications of sitter arrival and departure times.</p>
            <div class="section5__wrapper grid">
                <div class="grid__item">
                    <div class="circle"><img src="assets/img/Flower-icon.svg"></div>
                    <p class="grid__item__title">Dog walk</p>
                    <p class="grid__item__text">We'll take your pup for a 30 minute walk and make sure he or she has fresh food and water.</p>
                    <div class="price">
                        <p class="price__value">$15</p>
                        <p class="price__detail"><b>PER 30 MIN WALK</b></p>
                    </div>
                </div>
                <div class="grid__item">
                    <div class="circle"><img src="assets/img/Wave-icon.svg"></div>
                    <p class="grid__item__title">Drop-in visit</p>
                    <p class="grid__item__text">We'll stop by to snuggle, feed, and play with your pets in the comfort of their own home.</p>
                    <div class="price">
                        <p class="price__value">$15</p>
                        <p class="price__detail"><b>PER 30 MIN VISIT</b></p>
                    </div>
                </div>
                <div class="grid__item">
                    <div class="circle"><img src="assets/img/Home-icon.svg"></div>
                    <p class="grid__item__title">House sitting</p>
                    <p class="grid__item__text">We'll stay overnight with your pets to make sure they have round-the-clock love.</p>
                    <div class="price">
                        <p class="price__value">$45</p>
                        <p class="price__detail"><b>PER NIGHT</b></p>
                    </div>
                </div>
            </div>
            <a href="#" class="section5__button">Schedule a visit</a>
        </div>
        
        <div class="footer">
            <p>Contact</p>
            <p class="phone-number">481-624-3240</p>
            <a href="#">Email us</a>
        </div>
    </div>
</body>
</html>

<script>
    if(($(window).width() >= 1360) && ($(window).width() <= 2000)){
        $("#container").css("width", $(window).width());
        $("#section1__bg").css("width", $(window).width());
    }
</script>