<!DOCTYPE html>
<html lang="en">

<head>
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign-up</title>

    <link rel="icon" href="assets/img/Group247.svg">
    <link rel="stylesheet" href="assets/css/signup.css">
    <!-- Font -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Basic&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">
</head>

<body>
    <div class="container">
        <div class="aside">
            <img class="aside__bg" src="assets/img/signup-1.jpg">
            <div class="aside__container">
                <div class="aside__logo mb-66">
                    <div class="aside__circle"><img class="aside__icon" src="assets/img/Group247.svg"></div>
                    <p class="aside__title aside__title_logo">PAWTASTIC</p>
                </div>

                <h2 class="aside__title aside__title_list">Why our service?</h2>
                <ul class="aside__list">
                    <li>We're animal lovers backed by insurance and experience</li>
                    <li>Powered by tech, so you can book and pay from our app</li>
                    <li>Updates and pics for every visit keep you in the loop</li>
                </ul>
            </div>
        </div>

        <div class="section">
            <div class="section__container">
                <h1 class="section__title mb-40">First, let's make sure we serve your area.</h1>
                <form action="#">
                    <input class="section__input" type="number" id="zip-code" name="zip-code" placeholder="Zip Code" oninput="process(this)" onwheel="event.target.blur()">
                </form>
            </div>
        </div>
    </div>
    
    <div class="navigation">
        <div class="navigation__container">
            <a class="back" href="index.php">
                <div class="navigation__button">back</div>
            </a>
            <a class="next" href="signup-2.php">
                <div class="navigation__button">next</div>
            </a>
        </div>
    </div>
</body>

</html>

<script>
    function process(input) {
        let value = input.value;
        let numbers = value.replace(/[^0-9]/g, "");
        input.value = numbers;
    }
</script>